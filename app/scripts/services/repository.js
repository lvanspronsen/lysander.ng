'use strict';

angular.module('lysander.ngApp')
.factory('Repository', function($http, Query, Auth, lyconfig) {

	/* Object repositories */
	var Repository = function(path) {
		this.path = path;
	};

	/* Retrieves the url of a service */
	Repository.prototype.getServiceUrl = function(service) {
		var url = lyconfig.apiUrl + this.path + '/' + service;
		if(Auth.isLoggedIn()) {
			var token = Auth.getToken();
			url += '?token=' + token.id;
		}
		return url;
	};

	/* Sends a request to the server */
	Repository.prototype.send = function(url, obj, out) {
		var promise = $http.post(url, obj);
		return promise.then(function(result) {
			if(out) {
				angular.copy(result.data, out);
			}
			return result.data;
		});
	};

	/* Returns all accessible values from the repository */
	Repository.prototype.all = function(out) {
		var url = this.getServiceUrl('all');
		return this.send(url, {}, out);
	};

	/* Queries the repository for a subset of objects */
	Repository.prototype.query = function(out) {
		return new Query(this, false, out);
	};

	Repository.prototype._query = function(query, out) {
		var url = this.getServiceUrl('query');
		return this.send(url, query, out);
	};

	/* Queries the repository for the first object matching a query */
	Repository.prototype.first = function(out) {
		var q = new Query(this, true, out);
		q.skip(0).take(1);
		return q;
	};

	Repository.prototype._first = function(query, out) {
		var url = this.getServiceUrl('first');
		return this.send(url, query, out);
	};

	/* Finds a single existing object */
	Repository.prototype.find = function(id, out) {
		var url = this.getServiceUrl('find');
		return this.send(url, { id : id }, out);
	};

	/* Inserts a new object */
	Repository.prototype.insert = function(obj, out) {
		var url = this.getServiceUrl('insert');
		return this.send(url, obj, out);
	};

	/* Updates an object */
	Repository.prototype.update = function(obj, out) {
		var url = this.getServiceUrl('update');
		return this.send(url, obj, out);
	};

	/* Deletes an object */
	Repository.prototype.del = function(obj) {
		var url = this.getServiceUrl('delete');
		return this.send(url, { id : obj.id }, {});
	};

	/* Applies a changeset to the repository */
	Repository.prototype.changeset = function(changeset) {
		var url = this.getServiceUrl('changeset');
		return this.send(url, changeset, {});
	};

	return Repository;
});
