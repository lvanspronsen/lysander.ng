'use strict';

angular.module('lysander.ngApp')
	.factory('Query', function() {

	/* Allows construction of client side queries */
	var Query = function(repo, first, out) {
		this.repo = repo;
		this.first = first;
		this.out = out;
		this.query = {};
	};

	/* sets the view that is being queried */
	Query.prototype.view = function(view) {
		this.query.view = view;
		return this;
	};

	/* Fields query result on a fields value */
	Query.prototype.where = function(field, op, value) {
		if(!value) {
			return this;
		}

		if(!this.query.filters) {
			this.query.filters = [];
		}

		this.query.filters.push({
			field : field,
			op : op,
			value : value
		});

		return this;
	};

	/* Performs an 'OR' of multiple query conditions */
	Query.prototype.any = function(cb) {
		if(!this.query.filters) {
			this.query.filters = [];
		}

		var nq = new Query(this.repo, false, null);
		cb(nq);
		if(nq.query.filters) {
			this.query.filters.push({
				group : true,
				filters : nq.query.filters
			});
		}
		return this;
	};

	/* Performs an 'AND' of multiple query conditions */
	Query.prototype.all = function(cb) {
		if(!this.query.filters) {
			this.query.filters = [];
		}

		var nq = new Query(this.repo, false, null);
		cb(nq);
		if(nq.query.filters) {
			this.query.filters.push({
				group : true,
				filters : nq.query.filters
			});
		}
		return this;
	};

	/* Orders query results by a field */
	Query.prototype.orderBy = function(field, dir) {
		if(!this.query.orderBy) {
			this.query.orderBy = [];
		}

		this.query.orderBy.push({
			field : field,
			dir : dir
		});

		return this;
	};

	/* The number of rows to skip */
	Query.prototype.skip = function(skip) {
		this.query.skip = skip;
		return this;
	};

	/* The number of rows to take */
	Query.prototype.take = function(take) {
		this.query.take = take;
		return this;
	};

	/* Changes the query to retrieve the number of rows matching the filters */
	Query.prototype.count = function() {
		this.query.count = true;
		return this;
	};

	/* Runs the query */
	Query.prototype.run = function() {
		if(this.first) {
			return this.repo._first(this.query, this.out);
		} else {
			return this.repo._query(this.query, this.out);
		}
	};

	return Query;
});
