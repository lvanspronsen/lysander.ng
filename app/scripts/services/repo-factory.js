'use strict';

angular.module('lysander.ngApp')
.service('RepoFactory', function Repofactory(Repository) {

	this.create = function(path) {
		return new Repository(path);
	};

});
