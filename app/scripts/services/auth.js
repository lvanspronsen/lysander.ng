'use strict';

angular.module('lysander.ngApp')
  .service('Auth', function Auth($http, lyconfig) {

  	var token = null;
  	var loggedInCallbacks = [];
  	var loggedOutCallbacks = [];
  	var authErrorCallbacks = [];

  	// fires all callbacks for an event
  	var on = function(callbacks, arg1, arg2) {
  		for(var i = 0; i < callbacks.length; i++) {
  			callbacks[i](arg1, arg2);
  		}
  	};

  	// authenticates the current user with the supplied credentials
  	this.authenticate = function(credentials) {
  		var url = lyconfig.apiUrl + 'auth/authenticate';
  		var prom = $http.post(url, credentials);

  		prom.then(function(result) {
  			token = result.data;
  			on(loggedInCallbacks, token);
  		}, function(err) {
  			on(authErrorCallbacks, err.data);
  		});
  	};

  	// retrieves the current auth token
  	this.getToken = function() {
  		return token;
  	};

  	// checks whether the user is authenticated
  	this.isLoggedIn = function() {
  		return token !== null;
  	};

  	// registers a callback to fire when a user logs in
  	this.loggedIn = function(cb) {
  		loggedInCallbacks.push(cb);
  		return function() {
  			var index = loggedInCallbacks.indexOf(cb);
  			loggedInCallbacks.splice(index, 1);
  		};
  	};

  	// registers a callback to fire when a user logs out
  	this.loggedOut = function(cb) {
  		loggedOutCallbacks.push(cb);
  		return function() {
  			var index = loggedOutCallbacks.indexOf(cb);
  			loggedOutCallbacks.splice(index, 1);
  		};
  	};

  	this.authError = function(cb) {
  		authErrorCallbacks.push(cb);
  		return function() {
  			var index = authErrorCallbacks.indexOf(cb);
  			authErrorCallbacks.splice(index, 1);
  		};
  	};

  });
