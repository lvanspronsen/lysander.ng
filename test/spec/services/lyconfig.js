'use strict';

describe('Service: lyconfig', function () {

  // load the service's module
  beforeEach(module('lysander.ngApp'));

  // instantiate service
  var lyconfig;
  beforeEach(inject(function (_lyconfig_) {
    lyconfig = _lyconfig_;
  }));

  it('should do something', function () {
    expect(!!lyconfig).toBe(true);
  });

});
